﻿using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class User:IEntity
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? CustomerId { get; set; }
        public virtual Customer Customer { get; set; }


    }
}
