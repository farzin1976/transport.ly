﻿
using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class AirPort:IAirPort
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
