﻿using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class Customer:IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Tel { get; set; }
        public string Address { get; set; }
    }
}
