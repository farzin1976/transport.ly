﻿using System;
using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class FlightItinerari: IFlightItinerari
    {
        public int Id { get; set; }
        public DateTime FlightDate { get; set; }
        public int AirPlainId { get; set; }
        public virtual AirPlain AirPlain { get; set; }
        public int AirPortId { get; set; }
        public virtual AirPort AirPort { get; set; }
    }
}
