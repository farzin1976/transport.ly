﻿using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class Role:IEntity
    {
        public int Id { get; set; }
        public string RoleName { get; set; }
    }
}
