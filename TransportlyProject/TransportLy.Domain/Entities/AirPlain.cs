﻿using TransportLy.Domain.Contracts;

namespace TransportLy.Domain.Entities
{
    public class AirPlain : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Capacity { get; set; }
    }
}
