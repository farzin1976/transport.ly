﻿using System;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Enumes;

namespace TransportLy.Domain.Entities
{
    public class Order:IOrder  
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public string OrderCode { get; set; }
        public int Quantity { get; set; }
        public ShippingTypeEnum ShippingType { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime OrderDate { get; set; }
        public bool? Accepted { get; set; }
        public DateTime? AcceptDate { get; set; }
        public bool? Shipped { get; set; }
        public DateTime? ShippingDate { get; set; }
        public int AirportId { get; set; }
        public virtual AirPort AirPort { get; set; }
        public int? FlightItinerariId { get; set; }
        public virtual FlightItinerari FlightItinerari { get; set; }
    }
}
