﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TransportLy.Domain.Contracts
{
    public interface IRepository<T> where T : class
    {
        IEnumerable<T> Find();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
