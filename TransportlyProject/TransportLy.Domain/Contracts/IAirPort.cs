﻿namespace TransportLy.Domain.Contracts
{
    public interface IAirPort:IEntity
    {
         string Name { get; set; }
    }
}
