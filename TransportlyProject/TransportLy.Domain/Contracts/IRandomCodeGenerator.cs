﻿namespace TransportLy.Domain.Contracts
{
    public interface IRandomCodeGenerator
    {
        string GenerateCode(int length);
    }
}
