﻿using System;
using TransportLy.Domain.Enumes;

namespace TransportLy.Domain.Contracts
{
    public interface IOrder : IEntity
    {
         int CustomerId { get; set; }
         string OrderCode { get; set; }
         int Quantity { get; set; }
         ShippingTypeEnum ShippingType { get; set; }
         DateTime SubmitDate { get; set; }
         DateTime OrderDate { get; set; }
         bool? Accepted { get; set; }
         DateTime? AcceptDate { get; set; }
         bool? Shipped { get; set; }
         DateTime? ShippingDate { get; set; }
         int AirportId { get; set; }
         int? FlightItinerariId { get; set; }
    }
}
