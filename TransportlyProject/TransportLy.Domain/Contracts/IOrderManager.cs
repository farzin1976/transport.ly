﻿using System;

namespace TransportLy.Domain.Contracts
{
    public interface IOrderManager : IManager<IOrder>
    {
        IOrder Find(string customerCode);
        int QuantitySum(int airportId, DateTime date);
    }
}
