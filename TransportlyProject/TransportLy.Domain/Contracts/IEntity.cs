﻿
namespace TransportLy.Domain.Contracts
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
