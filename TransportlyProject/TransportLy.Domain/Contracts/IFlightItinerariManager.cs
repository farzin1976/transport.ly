﻿
using System.Collections.Generic;

namespace TransportLy.Domain.Contracts
{
    public interface IFlightItinerariManager:IManager<IFlightItinerari>
    {
        IFlightItinerari GetFlighOrders(int id);
        bool AddOrderToFlight(int flightId, int OrderId);
    }
}
