﻿using System;
using TransportLy.Domain.Entities;

namespace TransportLy.Domain.Contracts
{
    public interface IFlightItinerari : IEntity
    {
        AirPort AirPort { get; set; }
        AirPlain AirPlain { get; set; }
        int AirPlainId { get; set; }
        int AirPortId { get; set; }
        DateTime FlightDate { get; set; }


    }
}
