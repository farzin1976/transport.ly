﻿namespace TransportLy.Domain.Enumes
{
    public enum ShippingTypeEnum
    {
        SameDay=1,
        NextDay=2,
        Regular=3
    }
}
