﻿using System;

namespace TransportLy.Application.Exceptions
{
    public class FlightOrderException:Exception
    {
        private static readonly string DefaultMessage = "you can not add this order to the flight";
        public FlightOrderException():base(DefaultMessage)
        {

        }
    }
}
