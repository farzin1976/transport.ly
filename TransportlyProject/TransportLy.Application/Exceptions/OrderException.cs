﻿using System;

namespace TransportLy.Application.Exceptions
{
    public class OrderException:Exception
    {
        private static readonly string DefaultMessage = "we can not process your order for this date";
        private readonly string _orderCode;
        private readonly DateTime _orderDate;
        private readonly int _quantity;
        public OrderException(string orderCode,DateTime orderdate,int quantity) :base(DefaultMessage)
        {
            _orderDate = orderdate;
            _quantity = quantity;
            _orderCode = orderCode;
        }

        

    }
}
