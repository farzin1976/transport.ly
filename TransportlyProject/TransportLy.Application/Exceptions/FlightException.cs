﻿using System;

namespace TransportLy.Application.Exceptions
{
    public class FlightException:Exception
    {
        private static readonly string DefaultMessage = "your flight values are incorrect";
        private readonly DateTime _flightDate;
        private string _airplainName;
        private string _airportName;
        public FlightException(DateTime flightDate,string airplainName,string airportName) :base(DefaultMessage)
        {
            _flightDate = flightDate;
            _airplainName = airplainName;
            _airportName = airportName;
        }
    }
}
