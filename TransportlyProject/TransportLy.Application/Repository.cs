﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TransportLy.Persistence;
using System.Linq;
using TransportLy.Domain.Contracts;

namespace TransportLy.Application
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly TransportLyDbContext _dbCotext;
        public Repository(TransportLyDbContext dbContext)
        {
            _dbCotext = dbContext;
        }
        public void Create(T entity)
        {
            _dbCotext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            _dbCotext.Set<T>().Remove(entity);
        }

        public IEnumerable<T> Find()
        {
            return _dbCotext.Set<T>();
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _dbCotext.Set<T>().Where(expression);
        }

        public void Save()
        {
            _dbCotext.SaveChanges();
        }

        public void Update(T entity)
        {
            _dbCotext.Set<T>().Update(entity);
        }
    }
}
