﻿using System;
using System.Text;
using TransportLy.Domain.Contracts;

namespace TransportLy.Application.Helpers
{
    public class SimpleCodeGenerator : IRandomCodeGenerator
    {
        public string GenerateCode(int length)
        {
            Random random = new Random();
            string characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            StringBuilder result = new StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                result.Append(characters[random.Next(characters.Length)]);
            }
            return result.ToString();
        }
    }
}
