﻿using System;
using System.Collections.Generic;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.DTOs
{
    public class FlightOrdersDTO: IFlightItinerari
    {
        public int AirPlainId { get; set; }
        public int AirPortId { get; set; }
        public int Id { get; set; }
        public DateTime FlightDate { get; set; }
        public AirPort AirPort { get; set; }
        public AirPlain AirPlain { get; set; }
        public int OrdersCount { get; set; }
        public int OrdersSum { get; set; }
        public IEnumerable<IOrder> AssignedOrders { get; set; }
        public IEnumerable<IOrder> PotentialOrders { get; set; }

    }
}
