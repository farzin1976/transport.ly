﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.DTOs
{
    public class UserDTO
    {
        
        public string RoleName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int? CustomerId { get; set; }
        public string CustomerName { get; set; }

        private static Expression<Func<User, UserDTO>> Projection
        {
            get
            {
                return user => new UserDTO
                {
                    RoleName=user.Role.RoleName,
                    UserName=user.UserName,
                    Password=user.Password,
                    CustomerName=user.Customer.Name,
                    CustomerId=user.CustomerId

                };
            }
        }

        private static Expression<Func<UserDTO, User>> Conversion
        {
            get
            {
                return user => new User
                {
                    Password=user.Password,
                    UserName=user.UserName
                };
            }
        }

        public static User MapToDb(UserDTO user)
        {
            return Conversion.Compile().Invoke(user);
        }
        public static UserDTO MapToEntity(User user)
        {
            return Projection.Compile().Invoke(user);
        }
        public static IEnumerable<UserDTO> CreateList(IEnumerable<User> users)
        {
            HashSet<UserDTO> userDtos = new HashSet<UserDTO>();
            if (users != null)
            {
                foreach (var item in users)
                {
                    userDtos.Add(Projection.Compile().Invoke(item));
                }
            }
            return userDtos;
        }


    }
}
