﻿using System;
using System.Linq.Expressions;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;
using TransportLy.Domain.Enumes;
using System.ComponentModel.DataAnnotations;

namespace TransportLy.Application.DTOs
{
    public class OrderDTO:IOrder
    {
        
        public OrderDTO()
        {

        }
        public OrderDTO(int customerId,IRandomCodeGenerator randomCodeGenerator)
        {
            Accepted = false;
            Shipped = false;
            OrderDate = DateTime.Now;
            CustomerId = customerId;
            Quantity = 1;
            SubmitDate = DateTime.Now;
            OrderCode = randomCodeGenerator.GenerateCode(10);

        }
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string OrderCode { get; set; }
        
        public int Quantity { get; set; }
        public ShippingTypeEnum ShippingType { get; set; }
        public DateTime SubmitDate { get; set; }
        [DataType(DataType.Date)]
        [Display(Name = "Order Date")]
        public DateTime OrderDate { get; set; }
        public bool? Accepted { get; set; }
        public DateTime? AcceptDate { get; set; }
        public bool? Shipped { get; set; }
        public DateTime? ShippingDate { get; set; }
        [Display(Name = "Destination")]
        public int AirportId { get; set; }
        public int? FlightItinerariId { get; set; }

        private static Expression<Func<Order, IOrder>> Projection
        {
            get
            {
                return Order => new OrderDTO
                {
                    Id=Order.Id,
                    CustomerId=Order.CustomerId,
                    OrderCode = Order.OrderCode,
                    Quantity=Order.Quantity,
                    ShippingType=Order.ShippingType,
                    SubmitDate=Order.SubmitDate,
                    Accepted=Order.Accepted,
                    AcceptDate=Order.AcceptDate,
                    Shipped=Order.Shipped,
                    ShippingDate=Order.ShippingDate,
                    AirportId=Order.AirportId,
                    FlightItinerariId=Order.FlightItinerariId

                };
            }
        }

        private static Expression<Func<IOrder, Order>> Conversion
        {
            get
            {
                return Order => new Order
                {
                    CustomerId=Order.CustomerId,
                    OrderCode = Order.OrderCode,
                    Quantity = Order.Quantity,
                    ShippingType = Order.ShippingType,
                    SubmitDate = Order.SubmitDate,
                    OrderDate=Order.OrderDate,
                    Accepted = Order.Accepted,
                    AcceptDate = Order.AcceptDate,
                    Shipped = Order.Shipped,
                    ShippingDate = Order.ShippingDate,
                    AirportId = Order.AirportId,
                    FlightItinerariId = Order.FlightItinerariId

                };
            }
        }

       

        public static Order MapToDb(IOrder order)
        {
            return Conversion.Compile().Invoke(order);
        }
        public static IOrder MapToEntity(Order order)
        {
            return Projection.Compile().Invoke(order);
        }

    }

  


}
