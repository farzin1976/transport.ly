﻿using TransportLy.Domain.Contracts;
using System;
using System.Linq.Expressions;
using TransportLy.Domain.Entities;
using System.Collections;
using System.Collections.Generic;

namespace TransportLy.Application.DTOs
{
    public class FlightItinerariDTO : IFlightItinerari
    {
        public int AirPlainId { get; set; }
        public int AirPortId { get; set; }
        public int Id { get ; set ; }
        public DateTime FlightDate { get; set; }
        public AirPort AirPort { get; set; }
        public AirPlain AirPlain { get; set; }


        private static Expression<Func<FlightItinerari, IFlightItinerari>> Projection
        {
            get
            {
                return flight => new FlightItinerariDTO
                {
                    Id = flight.Id,
                    AirPlainId=flight.AirPlainId,
                    AirPortId=flight.AirPortId,
                    FlightDate=flight.FlightDate,
                    AirPlain=flight.AirPlain,
                    AirPort=flight.AirPort
                    

                };
            }
        }

        private static Expression<Func<IFlightItinerari, FlightItinerari>> Conversion
        {
            get
            {
                return flight => new FlightItinerari
                {
                    Id = flight.Id,
                    AirPlainId = flight.AirPlainId,
                    AirPortId = flight.AirPortId,
                    FlightDate = flight.FlightDate,
                    AirPort=flight.AirPort,
                    AirPlain=flight.AirPlain
                };
            }
        }

      
        public static FlightItinerari MapToDb(IFlightItinerari flight)
        {
            return Conversion.Compile().Invoke(flight);
        }
        public static IFlightItinerari MapToEntity(FlightItinerari flight)
        {
            return Projection.Compile().Invoke(flight);
        }
        public static List<FlightItinerariDTO> CreateList(IEnumerable<FlightItinerari> flights)
        {
            List<FlightItinerariDTO> retvalue=new List<FlightItinerariDTO>();
            foreach (var item in flights)
            {
                retvalue.Add(new FlightItinerariDTO()
                {
                    AirPlain = item.AirPlain,
                    AirPort = item.AirPort,
                    FlightDate = item.FlightDate,
                    Id = item.Id

                });

            }
            return retvalue;
        }

    }
}
