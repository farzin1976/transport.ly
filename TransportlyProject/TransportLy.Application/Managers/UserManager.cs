﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using TransportLy.Application.DTOs;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.Orders
{
    public class UserManager : IManager<User>
    {
        
        private readonly IRepository<User> _repository;
        public UserManager(IRepository<User> repository)
        {
            
            _repository = repository;
        }

        public User Add(User item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> Find()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> Find(Expression<Func<User, bool>> expression)
        {
            return _repository.Find(expression);
        }

        public User Find(int id)
        {
            throw new NotImplementedException();
        }

        public User Modify(User item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
