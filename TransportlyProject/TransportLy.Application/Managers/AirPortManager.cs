﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.Managers
{
    public class AirPortManager : IManager<AirPort>
    {
        private readonly IRepository<AirPort> _repository;
        public AirPortManager(IRepository<AirPort> repository)
        {
            _repository = repository;
        }
        public AirPort Add(AirPort item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AirPort> Find()
        {
            return _repository.Find();
        }

        public IEnumerable<AirPort> Find(Expression<Func<AirPort, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public AirPort Find(int id)
        {
            throw new NotImplementedException();
        }

        public AirPort Modify(AirPort item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
