﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TransportLy.Application.DTOs;
using TransportLy.Application.Exceptions;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;
using System.Linq;

namespace TransportLy.Application.Managers
{
    public class FlightItinerariManager : IFlightItinerariManager
    {
        private readonly IRepository<FlightItinerari> _repository;
        private readonly IRepository<Order> _orderRepository;
        private const int MaxQuantity = 20;
        public FlightItinerariManager(IRepository<FlightItinerari> repository, 
                                      IRepository<Order> orderRepository)
        {
            _repository = repository;
            _orderRepository = orderRepository;
        }
        private bool AddCondtions(IFlightItinerari flight)
        {
            if (flight.FlightDate.Date < DateTime.Now.Date)
                return false;
            if (_repository.Find(c => c.FlightDate.Date == flight.FlightDate.Date && c.AirPlainId == flight.AirPlainId).Any() )
                return false;
            if (_repository.Find(c => c.FlightDate.Date == flight.FlightDate.Date && c.AirPortId == flight.AirPortId).Any())
                return false;
            if (_repository.Find(c => c.FlightDate.Date == flight.FlightDate.Date && c.AirPortId == flight.AirPortId && c.AirPlainId == flight.AirPlainId).Any())
                return false;
            return true;
        }
        public IFlightItinerari Add(IFlightItinerari item)
        {
            if (AddCondtions(item))
            {
                var flight = FlightItinerariDTO.MapToDb(item);
                _repository.Create(flight);
                _repository.Save();
                return flight;
            }
            else
            {
                throw new FlightException(item.FlightDate, "", "");
            }
        }

        public IEnumerable<IFlightItinerari> Find()
        {
            var flights=_repository.Find().OrderBy(c=>c.FlightDate).ThenBy(c=>c.AirPortId).ThenBy(c=>c.AirPlainId).ToList();
            var flightDTOs = FlightItinerariDTO.CreateList(flights).ToList();
            return flightDTOs;
        }

        public IEnumerable<IFlightItinerari> Find(Expression<Func<IFlightItinerari, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public IFlightItinerari Find(int id)
        {
           return _repository.Find(c=>c.Id==id).FirstOrDefault();
            
        }

        public IFlightItinerari Modify(IFlightItinerari item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public IFlightItinerari GetFlighOrders(int id)
        {
            FlightOrdersDTO flightOrdersDTO = new FlightOrdersDTO();
            var flight = _repository.Find(c => c.Id == id).FirstOrDefault();
            var potential = _orderRepository.Find(c => c.FlightItinerariId == null && c.AirportId == flight.AirPortId);
            var assigned = _orderRepository.Find(c => c.FlightItinerariId == id);
            var count = _orderRepository.Find(c => c.FlightItinerariId == id).Count();
            var sum = _orderRepository.Find(c => c.FlightItinerariId == id).Sum(c => c.Quantity);
            flightOrdersDTO.OrdersSum = sum;
            flightOrdersDTO.OrdersCount = count;
            flightOrdersDTO.AirPlain = flight.AirPlain;
            flightOrdersDTO.AirPort = flight.AirPort;
            flightOrdersDTO.Id = flight.Id;
            flightOrdersDTO.FlightDate = flight.FlightDate;
            flightOrdersDTO.PotentialOrders = potential;
            flightOrdersDTO.AssignedOrders = assigned;
            return flightOrdersDTO;
        }

        public bool AddOrderToFlight(int flightId, int orderId)
        {
            var sum = _orderRepository.Find(c => c.FlightItinerariId == flightId).Sum(s => s.Quantity);
            var order=_orderRepository.Find(c => c.Id == orderId).FirstOrDefault();
            if(sum+order.Quantity>MaxQuantity)
            {
                throw new FlightOrderException();
            }
            if(order!=null)
            {
                order.FlightItinerariId = flightId;
                order.AcceptDate = DateTime.Now;
                order.Accepted = true;
                _orderRepository.Update(order);
                _orderRepository.Save();
            }
            return true;
        }
    }
}
