﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.Managers
{
    public class AirPlainManager : IManager<AirPlain>
    {
        private readonly IRepository<AirPlain> _repository;
        public AirPlainManager(IRepository<AirPlain> repository)
        {
            _repository = repository;
        }
        public AirPlain Add(AirPlain item)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<AirPlain> Find()
        {
            return _repository.Find();
        }

        public IEnumerable<AirPlain> Find(Expression<Func<AirPlain, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public AirPlain Find(int id)
        {
            throw new NotImplementedException();
        }

        public AirPlain Modify(AirPlain item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }
    }
}
