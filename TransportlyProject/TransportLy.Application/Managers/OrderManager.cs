﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using TransportLy.Application.DTOs;
using TransportLy.Application.Exceptions;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.Application.Orders
{
    public class OrderManager :IOrderManager
    {
        private readonly IRepository<Order> _repository;
        private const int MaxQuantity = 20;
        public OrderManager( IRepository<Order> repository)
        {
            _repository = repository;
        }
        private bool AddCondtions(IOrder order)
        {
            if (order.OrderDate == DateTime.MinValue)
                return false;
            if (order.OrderDate.Date < DateTime.Now.Date)
                return false;
            if (order.Quantity > MaxQuantity)
                return false;
            if (order.OrderDate.Date < DateTime.Now.Date)
                return false;
            if (order.ShippingType == Domain.Enumes.ShippingTypeEnum.NextDay && QuantitySum(order.AirportId, order.OrderDate.AddDays(1)) + order.Quantity > MaxQuantity)
            {
                return false;
             }
            if (order.ShippingType == Domain.Enumes.ShippingTypeEnum.SameDay && QuantitySum(order.AirportId, order.OrderDate) + order.Quantity > MaxQuantity)
            {
                return false;
            }
            return true;
        }
      
        public IOrder Add(IOrder item)
        {
            if (AddCondtions(item))
            {
                var order = OrderDTO.MapToDb(item);
                _repository.Create(order);
                _repository.Save();
                return order;
            }
            else
            {
                throw new OrderException(item.OrderCode, item.OrderDate, item.Quantity);
            }
        }

        public IOrder Find(string customerCode)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IOrder> Find()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IOrder> Find(Expression<Func<IOrder, bool>> expression)
        {
            throw new NotImplementedException();
        }

        public IOrder Find(int id)
        {
            throw new NotImplementedException();
        }

        public IOrder Modify(IOrder item)
        {
            throw new NotImplementedException();
        }

        public bool Remove(int id)
        {
            throw new NotImplementedException();
        }

        public int QuantitySum(int airportId, DateTime date)
        {
            var orders=_repository.Find(c =>(c.OrderDate.Date == date.Date &&
                                             c.AirportId == airportId &&
                                             c.ShippingType==Domain.Enumes.ShippingTypeEnum.SameDay) || 
                                             (c.OrderDate.Date == date.Date.AddDays(-1) &&
                                              c.AirportId == airportId &&
                                              c.ShippingType == Domain.Enumes.ShippingTypeEnum.NextDay));
            int sum = 0;
            foreach (var item in orders)
            {
                sum += item.Quantity;
            }
            return sum;
        }
    }
}
