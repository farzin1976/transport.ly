﻿namespace TransportLy.Contract
{
    public interface IEntity
    {
        int Id { get; set; }
    }
}
