﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TransportLy.Contract
{
    public interface IManager<T> where T:IEntity
    {
        IEnumerable<T> Find();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        T Find(int id);
        T Add(T item);
        T Modify(T item);
        bool Remove(int id);
    }
}
