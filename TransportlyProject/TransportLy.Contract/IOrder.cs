﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransportLy.Contract
{
    public interface IOrder:IEntity
    {
         string OrderCode { get; set; }
    }
}
