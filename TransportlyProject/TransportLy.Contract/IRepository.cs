﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace TransportLy.Contract
{
    public interface IRepository<T> where T :IEntity
    {
        IEnumerable<T> Find();
        IEnumerable<T> Find(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
        void Save();
    }
}
