﻿namespace TransportLy.Contract
{
    public interface IRequestResponse
    {
        bool Success { get; set; }

        string Message { get; set; }

        object Data { get; set; }
    }
}
