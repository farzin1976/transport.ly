﻿namespace TransportLy.Contract
{
    public interface IOrderManager:IManager<IOrder>
    {
        IOrder Find(string customerCode);
    }
}
