﻿using Microsoft.Extensions.DependencyInjection;
using TransportLy.Application;
using TransportLy.Application.DTOs;
using TransportLy.Application.Helpers;
using TransportLy.Application.Managers;
using TransportLy.Application.Orders;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.MVC.Extentions
{
    public static class ServiceExtensions
    {
        public static void ConfigureOrderRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository<Order>, Repository<Order>>();
        }
        public static void ConfigureAirPortRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository<AirPort>, Repository<AirPort>>();
        }
        public static void ConfigureAirPlainRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository<AirPlain>, Repository<AirPlain>>();
        }
        public static void ConfigureUserRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository<User>, Repository<User>>();
        }
        public static void ConfigureFlightRepository(this IServiceCollection services)
        {
            services.AddScoped<IRepository<FlightItinerari>, Repository<FlightItinerari>>();
        }
        public static void ConfigureUserManager(this IServiceCollection services)
        {
            services.AddScoped<IManager<User>, UserManager>();
        }
        public static void ConfigureOrdeManager(this IServiceCollection services)
        {
            services.AddScoped<IOrderManager, OrderManager>();
        }
        public static void ConfigureAirPortManager(this IServiceCollection services)
        {
            services.AddScoped<IManager<AirPort>, AirPortManager>();
        }
        public static void ConfigureAirPlaintManager(this IServiceCollection services)
        {
            services.AddScoped<IManager<AirPlain>, AirPlainManager>();
        }
        public static void ConfigureFlightManager(this IServiceCollection services)
        {
            services.AddScoped<IFlightItinerariManager, FlightItinerariManager>();
        }
        public static void ConfigureRandomCodeGenerator(this IServiceCollection services)
        {
            services.AddSingleton<IRandomCodeGenerator, SimpleCodeGenerator>();
        }


    }
}
