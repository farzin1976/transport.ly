﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TransportLy.Domain.Contracts;

namespace TransportLy.MVC.Controllers
{
    public class ShippingManagerController : Controller
    {
        IFlightItinerariManager _manager;
        public ShippingManagerController(IFlightItinerariManager manager)
        {
            _manager = manager;
        }
        public IActionResult Flights()
        {
            return View(_manager.Find());
        }
        public IActionResult FlightOrders(int id)
        {
            return View(_manager.GetFlighOrders(id));
        }
    }
}