﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TransportLy.MVC.Models;
using TransportLy.Application.DTOs;
using TransportLy.Domain.Contracts;

namespace TransportLy.MVC.Controllers
{
    public class HomeController : Controller
    {
        IOrderManager _orderManager;
        public HomeController(IOrderManager orderManager)
        {
            _orderManager = orderManager;
        }
       
        public IActionResult Index()
        {
           
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
