﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TransportLy.Application.DTOs;
using TransportLy.Application.Exceptions;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.MVC.Controllers
{
    public class InventoryManagerController : Controller
    {
        IFlightItinerariManager _manager;
        IManager<AirPort> _airportManager;
        IManager<AirPlain> _airplainManager;

        public InventoryManagerController(IFlightItinerariManager manager, IManager<AirPort> airportManager, IManager<AirPlain> airplainManager)
        {
            _manager = manager;
            _airplainManager = airplainManager;
            _airportManager = airportManager;
        }
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult AddToFlight(int id,int flightId)
        {
            try
            {
                _manager.AddOrderToFlight(flightId, id);
                return RedirectToAction("FlightOrders", "InventoryManager", routeValues: new { @id = flightId });
            }
            catch (FlightOrderException ex)
            {
                //Log
                return RedirectToAction("FlightOrders", "InventoryManager", routeValues: new { @id = flightId });

            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public IActionResult flights()
        {
            return View(_manager.Find());
        }
        public IActionResult FlightOrders(int id)
        {

            return View(_manager.GetFlighOrders(id));
        }
        public IActionResult flight()
        {
            ViewBag.Airports = _airportManager.Find().ToList();
            ViewBag.Airplains = _airplainManager.Find().ToList();
            return View();
        }
        [HttpPost]
        public IActionResult flight(FlightItinerariDTO flight)
        {
            ViewBag.Airports = _airportManager.Find().ToList();
            ViewBag.Airplains = _airplainManager.Find().ToList();
            try
            {
                _manager.Add(flight);
                return RedirectToAction("flights", "InventoryManager");
            }
            catch (FlightException ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
            catch (Exception exp)
            {
                //Log message
                return View();
            }

            return View();
        }

    }
}