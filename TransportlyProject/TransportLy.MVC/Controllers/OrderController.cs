﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TransportLy.Application.DTOs;
using TransportLy.Application.Exceptions;
using TransportLy.Application.Helpers;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;

namespace TransportLy.MVC.Controllers
{
    public class OrderController : Controller
    {
        IOrderManager _orderManager;
        IManager<AirPort> _airportManager;
        IRandomCodeGenerator _randomCodeGenerator;
        public OrderController(IOrderManager orderManager, IManager<AirPort> airportManager,IRandomCodeGenerator randomCodeGenerator)
        {
            _orderManager = orderManager;
            _airportManager = airportManager;
            _randomCodeGenerator = randomCodeGenerator;
        }
        public IActionResult Index(int id)
        {
            ViewBag.Airports = _airportManager.Find().ToList();
            OrderDTO orderDTO = new OrderDTO(id,_randomCodeGenerator);
            return View(orderDTO);
        }
        [HttpPost]
        public IActionResult SubmitOrder(OrderDTO orderDTO)
        {
            try
            {
                orderDTO.SubmitDate = DateTime.Now;
                if (orderDTO.OrderDate == DateTime.MinValue) orderDTO.OrderDate = DateTime.Now;
                orderDTO.OrderCode = _randomCodeGenerator.GenerateCode(20);
                _orderManager.Add(orderDTO);
                ViewBag.Message = $"Your Order Is submited with this Code:[{orderDTO.OrderCode}]";
                return View();
            }
            catch(OrderException ex)
            {
                ViewBag.Message = ex.Message;
                return View();
            }
            catch (Exception )
            {
                //Log Error
                ViewBag.Message = $"OPPS Error !!!!";
                return View();

            }
            
        }
    }
}