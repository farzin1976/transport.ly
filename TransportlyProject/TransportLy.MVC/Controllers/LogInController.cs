﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using System.Text;
using TransportLy.Application.DTOs;
using TransportLy.Domain.Entities;
using TransportLy.Domain.Contracts;

namespace TransportLy.MVC.Controllers
{
    public class LogInController : Controller
    {

        IManager<User> _userManager;
        public LogInController(IManager<User> userManager)
        {
            _userManager = userManager;
        }

      
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(User user)
        {
            if (ModelState.IsValid)
            {
                var obj = _userManager.Find(a => a.UserName.Equals(user.UserName) && a.Password.Equals(user.Password)).FirstOrDefault();
                if (obj != null)
                {
                    HttpContext.Session.Set("UserName", Encoding.ASCII.GetBytes(obj.UserName));
                    HttpContext.Session.Set("Role", Encoding.ASCII.GetBytes(obj.RoleId.ToString()));
                    HttpContext.Session.Set("CustomerId", Encoding.ASCII.GetBytes(obj.CustomerId.HasValue ? obj.CustomerId.Value.ToString() : "0"));
                    return RedirectToAction("Index", "Home");


                }
            }
            return View(user);
        }
    }
}