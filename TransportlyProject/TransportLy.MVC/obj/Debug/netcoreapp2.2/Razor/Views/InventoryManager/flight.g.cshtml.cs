#pragma checksum "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "ff91c83777c98cfd40cc9bf65c93519fe1d5c43c"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_InventoryManager_flight), @"mvc.1.0.view", @"/Views/InventoryManager/flight.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/InventoryManager/flight.cshtml", typeof(AspNetCore.Views_InventoryManager_flight))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\_ViewImports.cshtml"
using TransportLy.MVC;

#line default
#line hidden
#line 2 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\_ViewImports.cshtml"
using TransportLy.MVC.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"ff91c83777c98cfd40cc9bf65c93519fe1d5c43c", @"/Views/InventoryManager/flight.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"996946230625633af69b651a6f2ea8f0ed4369bc", @"/Views/_ViewImports.cshtml")]
    public class Views_InventoryManager_flight : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TransportLy.Application.DTOs.FlightItinerariDTO>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
  
    ViewData["Title"] = "flight";

#line default
#line hidden
            BeginContext(98, 4, true);
            WriteLiteral("\r\n\r\n");
            EndContext();
#line 7 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
 using (Html.BeginForm("flight", "InventoryManager", FormMethod.Post))
{

#line default
#line hidden
            BeginContext(177, 63, true);
            WriteLiteral("    <fieldset>\r\n        <legend>New flight</legend>\r\n\r\n        ");
            EndContext();
            BeginContext(241, 23, false);
#line 12 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
   Write(Html.AntiForgeryToken());

#line default
#line hidden
            EndContext();
            BeginContext(264, 10, true);
            WriteLiteral("\r\n        ");
            EndContext();
            BeginContext(275, 28, false);
#line 13 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
   Write(Html.ValidationSummary(true));

#line default
#line hidden
            EndContext();
            BeginContext(303, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 14 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
         if (@ViewBag.Message != null)
        {

#line default
#line hidden
            BeginContext(356, 65, true);
            WriteLiteral("            <div style=\"border: 1px solid red\">\r\n                ");
            EndContext();
            BeginContext(422, 15, false);
#line 17 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(ViewBag.Message);

#line default
#line hidden
            EndContext();
            BeginContext(437, 22, true);
            WriteLiteral("\r\n            </div>\r\n");
            EndContext();
#line 19 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
        }

#line default
#line hidden
            BeginContext(470, 43, true);
            WriteLiteral("    <table>\r\n        <tr>\r\n            <td>");
            EndContext();
            BeginContext(514, 32, false);
#line 22 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.LabelFor(a => a.FlightDate));

#line default
#line hidden
            EndContext();
            BeginContext(546, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(570, 79, false);
#line 23 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.TextBoxFor(a => a.FlightDate, new { @class = "datefield", type = "date" }));

#line default
#line hidden
            EndContext();
            BeginContext(649, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(673, 44, false);
#line 24 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.ValidationMessageFor(a => a.FlightDate));

#line default
#line hidden
            EndContext();
            BeginContext(717, 52, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <td>");
            EndContext();
            BeginContext(770, 31, false);
#line 27 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.LabelFor(a => a.AirPortId));

#line default
#line hidden
            EndContext();
            BeginContext(801, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(825, 86, false);
#line 28 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.DropDownListFor(a => a.AirPortId, new SelectList(ViewBag.Airports, "Id", "Name")));

#line default
#line hidden
            EndContext();
            BeginContext(911, 54, true);
            WriteLiteral("</td>\r\n\r\n        </tr>\r\n        <tr>\r\n            <td>");
            EndContext();
            BeginContext(966, 32, false);
#line 32 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.LabelFor(a => a.AirPlainId));

#line default
#line hidden
            EndContext();
            BeginContext(998, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(1022, 88, false);
#line 33 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
           Write(Html.DropDownListFor(a => a.AirPlainId, new SelectList(ViewBag.Airplains, "Id", "Name")));

#line default
#line hidden
            EndContext();
            BeginContext(1110, 237, true);
            WriteLiteral("</td>\r\n        </tr>\r\n        <tr>\r\n            <td></td>\r\n            <td>\r\n                <input type=\"submit\" value=\"Submit fligh\" />\r\n            </td>\r\n            <td></td>\r\n        </tr>\r\n    </table>\r\n        \r\n    </fieldset>\r\n");
            EndContext();
#line 45 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\InventoryManager\flight.cshtml"
}

#line default
#line hidden
            BeginContext(1350, 2, true);
            WriteLiteral("\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TransportLy.Application.DTOs.FlightItinerariDTO> Html { get; private set; }
    }
}
#pragma warning restore 1591
