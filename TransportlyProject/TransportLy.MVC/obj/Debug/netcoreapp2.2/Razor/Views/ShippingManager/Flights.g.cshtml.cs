#pragma checksum "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "67e04b3fc5a3823ad303cbf2f512f57f146ae7a5"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_ShippingManager_Flights), @"mvc.1.0.view", @"/Views/ShippingManager/Flights.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/ShippingManager/Flights.cshtml", typeof(AspNetCore.Views_ShippingManager_Flights))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\_ViewImports.cshtml"
using TransportLy.MVC;

#line default
#line hidden
#line 2 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\_ViewImports.cshtml"
using TransportLy.MVC.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"67e04b3fc5a3823ad303cbf2f512f57f146ae7a5", @"/Views/ShippingManager/Flights.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"996946230625633af69b651a6f2ea8f0ed4369bc", @"/Views/_ViewImports.cshtml")]
    public class Views_ShippingManager_Flights : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<TransportLy.Application.DTOs.FlightItinerariDTO>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "FlightOrders", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
  
    ViewData["Title"] = "flights";

#line default
#line hidden
            BeginContext(112, 333, true);
            WriteLiteral(@"<h1>Flights</h1>
<table class=""table"">
    <thead>
        <tr>
            <th>
                Flight date
            </th>
            <th>
                Airport name
            </th>
            <th>
                Aiplane name
            </th>
            <th></th>
        </tr>
    </thead>
    <tbody>
");
            EndContext();
#line 22 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
            BeginContext(494, 60, true);
            WriteLiteral("            <tr>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(555, 50, false);
#line 26 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
               Write(Html.DisplayFor(modelItem => item.FlightDate.Date));

#line default
#line hidden
            EndContext();
            BeginContext(605, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(673, 47, false);
#line 29 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
               Write(Html.DisplayFor(modelItem => item.AirPort.Name));

#line default
#line hidden
            EndContext();
            BeginContext(720, 67, true);
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(788, 48, false);
#line 32 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
               Write(Html.DisplayFor(modelItem => item.AirPlain.Name));

#line default
#line hidden
            EndContext();
            BeginContext(836, 69, true);
            WriteLiteral("\r\n                </td>\r\n\r\n                <td>\r\n                    ");
            EndContext();
            BeginContext(905, 64, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "67e04b3fc5a3823ad303cbf2f512f57f146ae7a55934", async() => {
                BeginContext(958, 7, true);
                WriteLiteral("Details");
                EndContext();
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#line 36 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
                                                   WriteLiteral(item.Id);

#line default
#line hidden
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(969, 44, true);
            WriteLiteral("\r\n                </td>\r\n            </tr>\r\n");
            EndContext();
#line 39 "C:\Git_MyProjects\transport.ly\TransportlyProject\TransportLy.MVC\Views\ShippingManager\Flights.cshtml"
        }

#line default
#line hidden
            BeginContext(1024, 26, true);
            WriteLiteral("    </tbody>\r\n</table>\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<TransportLy.Application.DTOs.FlightItinerariDTO>> Html { get; private set; }
    }
}
#pragma warning restore 1591
