using NUnit.Framework;
using TransportLy.Application.Orders;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;
using TransportLy.Test.Mocks;
using System;
using TransportLy.Application.Exceptions;

namespace Tests
{
    public class OrderManagerTests

    {
        private IRepository<Order> _repository;
        private IOrderManager _manger;
       [SetUp]
        public void Setup()
        {
            _repository = new MockOrderRepository();
            _manger = new OrderManager(_repository);
        }

        [Test]
        public void WhenOrderDatePassed_ShouldThrowException()
        {
            Assert.Throws(typeof(OrderException), () => _manger.Add(new Order() { OrderDate = DateTime.Now.AddDays(-1) }));
        }
        [Test]
        public void WhenOrderQuantityGreaterThanMax_ShouldThrowException()
        {
            Assert.Throws(typeof(OrderException), () => _manger.Add(new Order() { Quantity = 21 }));
        }
        [Test]
        public void WhenOrderQuantityPlusSumGreaterThanMax_ShouldThrowException()
        {
            Assert.Throws(typeof(OrderException), () => _manger.Add(new Order() { Quantity = 3,OrderDate=DateTime.Now.Date ,ShippingType=TransportLy.Domain.Enumes.ShippingTypeEnum.SameDay,AirportId=1}));
        }
    }
}