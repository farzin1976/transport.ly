﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using TransportLy.Domain.Contracts;
using TransportLy.Domain.Entities;
using System.Linq;

namespace TransportLy.Test.Mocks
{
    public class MockOrderRepository : IRepository<Order>
    {
        private  List<Order> orders;
        public MockOrderRepository()
        {
            orders = new List<Order>();
            orders.Add(new Order() { OrderDate = DateTime.Now.Date, Quantity=19,AirportId=1 ,ShippingType=Domain.Enumes.ShippingTypeEnum.SameDay});
        }
        public void Create(Order entity)
        {
            throw new NotImplementedException();
        }

        public void Delete(Order entity)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> Find()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> Find(Expression<Func<Order, bool>> expression)
        {
            return orders;
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void Update(Order entity)
        {
            throw new NotImplementedException();
        }
    }
}
