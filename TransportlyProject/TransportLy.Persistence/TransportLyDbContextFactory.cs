﻿using Microsoft.EntityFrameworkCore;
using TransportLy.Persistence.Infrastructure;

namespace TransportLy.Persistence
{
    class TransportLyDbContextFactory : DesignTimeDbContextFactoryBase<TransportLyDbContext>
    {
        protected override TransportLyDbContext CreateNewInstance(DbContextOptions<TransportLyDbContext> options)
        {
            return new TransportLyDbContext(options);
        }
    }
}
