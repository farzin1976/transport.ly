﻿using Microsoft.EntityFrameworkCore;
using TransportLy.Domain.Entities;
using TransportLy.Persistence.Extentions;

namespace TransportLy.Persistence
{
    public class TransportLyDbContext : DbContext
    {
        public TransportLyDbContext(DbContextOptions<TransportLyDbContext> options) : base(options)
        {
            
        }
        public DbSet<AirPort> airPlains { get; set; }
        public DbSet<AirPort> airPorts { get; set; }
        public DbSet<Customer> customers { get; set; }
        public DbSet<FlightItinerari> flightItineraris { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<Role> roles { get; set; }
        public DbSet<User> users { get; set; }

       
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Search all Configurations and apply them 
            modelBuilder.ApplyAllConfigurations();

            modelBuilder.Entity<Role>().HasData(
               new Role() { Id = 1, RoleName = "customer" },
               new Role() { Id = 2, RoleName = "shippingManager" },
               new Role() { Id = 3, RoleName = "InventoryManager" }
               );

            modelBuilder.Entity<Customer>().HasData(
               new Customer() { Id = 1, Name = "customer1", Tel = "000000", Address = "XXX" },
               new Customer() { Id = 2, Name = "customer2", Tel = "000000", Address = "XXX" },
               new Customer() { Id = 3, Name = "customer3", Tel = "000000", Address = "XXX" }
               );

            modelBuilder.Entity<AirPlain>().HasData(
              new AirPlain() { Id = 1, Name = "AirPlain1",Capacity=20 },
              new AirPlain() { Id = 2, Name = "AirPlain2" ,Capacity=20},
              new AirPlain() { Id = 3, Name = "AirPlain3",Capacity=20 }
              );

            modelBuilder.Entity<AirPort>().HasData(
             new AirPort() { Id = 1, Name = "YUL" },
             new AirPort() { Id = 2, Name = "YYZ" },
             new AirPort() { Id = 3, Name = "YYC" },
             new AirPort() { Id = 4, Name = "YVR" }
             );
        }


    }
}
