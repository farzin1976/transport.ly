﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TransportLy.Persistence.Migrations
{
    public partial class addseeddata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "AirPlains",
                columns: new[] { "ID", "Capacity", "Name" },
                values: new object[,]
                {
                    { 1, 20, "AirPlain1" },
                    { 2, 20, "AirPlain2" },
                    { 3, 20, "AirPlain3" }
                });

            migrationBuilder.InsertData(
                table: "AirPorts",
                columns: new[] { "ID", "Name" },
                values: new object[,]
                {
                    { 1, "YUL" },
                    { 2, "YYZ" },
                    { 3, "YYC" },
                    { 4, "YVR" }
                });

            migrationBuilder.InsertData(
                table: "Customers",
                columns: new[] { "ID", "Address", "Name", "Tel" },
                values: new object[,]
                {
                    { 1, "XXX", "customer1", "000000" },
                    { 2, "XXX", "customer2", "000000" },
                    { 3, "XXX", "customer3", "000000" }
                });

            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "ID", "RoleName" },
                values: new object[,]
                {
                    { 1, "customer" },
                    { 2, "shippingManager" },
                    { 3, "InventoryManager" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AirPlains",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AirPlains",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "AirPlains",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "AirPorts",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "AirPorts",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "AirPorts",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "AirPorts",
                keyColumn: "ID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Customers",
                keyColumn: "ID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "ID",
                keyValue: 3);
        }
    }
}
