﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TransportLy.Persistence.Migrations
{
    public partial class addairplaincapacity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Capacity",
                table: "AirPlains",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Capacity",
                table: "AirPlains");
        }
    }
}
