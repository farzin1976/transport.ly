﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class AirPlainConfiguration : IEntityTypeConfiguration<AirPlain>
    {
        public void Configure(EntityTypeBuilder<AirPlain> builder)
        {

            builder.ToTable("AirPlains");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(10);

        }
    }
}
