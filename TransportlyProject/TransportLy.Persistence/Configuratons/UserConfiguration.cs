﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.UserName).IsRequired();
            builder.Property(e => e.UserName).HasMaxLength(10);
            builder.Property(e => e.Password).IsRequired();
            builder.Property(e => e.Password).HasMaxLength(50);



        }
    }
}
