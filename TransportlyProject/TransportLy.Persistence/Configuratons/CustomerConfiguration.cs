﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customers");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(10);
            builder.Property(e => e.Tel).IsRequired();
            builder.Property(e => e.Tel).HasMaxLength(10);
            builder.Property(e => e.Address).HasMaxLength(256);

        }
    }
}
