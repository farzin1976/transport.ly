﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.ToTable("Orders");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.OrderCode).IsRequired();
            builder.Property(e => e.OrderCode).HasMaxLength(30);
            builder.HasIndex(e => new { e.OrderCode}).IsUnique(true);
            builder.Property(e => e.Quantity).IsRequired();
            builder.Property(e => e.ShippingType).IsRequired();
            builder.Property(e => e.SubmitDate).IsRequired();
            builder.Property(e => e.OrderDate).IsRequired();
            builder.Property(e => e.AirportId).IsRequired();
           
        }
    }
}
