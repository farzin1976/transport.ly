﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class AirPortConfiguration : IEntityTypeConfiguration<AirPort>
    {
        public void Configure(EntityTypeBuilder<AirPort> builder)
        {
            builder.ToTable("AirPorts");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.Name).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(10);
        }
    }
}
