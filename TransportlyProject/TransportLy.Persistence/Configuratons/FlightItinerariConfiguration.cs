﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class FlightItinerariConfiguration : IEntityTypeConfiguration<FlightItinerari>
    {
        public void Configure(EntityTypeBuilder<FlightItinerari> builder)
        {
            builder.ToTable("FlightItineraries");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.FlightDate).IsRequired();
            builder.Property(e => e.AirPlainId).IsRequired();
            builder.HasIndex(e => new { e.AirPlainId, e.AirPortId, e.FlightDate }).IsUnique(true);
           







        }
    }
}
