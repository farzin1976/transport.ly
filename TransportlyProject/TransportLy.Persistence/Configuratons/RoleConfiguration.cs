﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TransportLy.Domain.Entities;

namespace TransportLy.Persistence.Configuratons
{
    internal class RoleConfiguration : IEntityTypeConfiguration<Role>
    {
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.ToTable("Roles");
            builder.Property(e => e.Id).HasColumnName("ID");
            builder.Property(e => e.RoleName).IsRequired();
            builder.Property(e => e.RoleName).HasMaxLength(20);

        }
    }
}
